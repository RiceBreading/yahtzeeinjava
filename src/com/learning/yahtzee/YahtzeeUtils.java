package com.learning.yahtzee;

import java.util.List;
import java.util.Scanner;

public class YahtzeeUtils {
    public static final String oneTopUnheld =         "|   |";
    public static final String oneMiddleUnheld =      "| O |";
    public static final String oneBottomUnheld =      "|   |";
    
    public static final String twoTopUnheld =         "|O  |";
    public static final String twoMiddleUnheld =      "|   |";
    public static final String twoBottomUnheld =      "|  O|";
    
    public static final String threeTopUnheld =       "|O  |";
    public static final String threeMiddleUnheld =    "| O |";
    public static final String threeBottomUnheld =    "|  O|";
    
    public static final String fourTopUnheld =        "|O O|";
    public static final String fourMiddleUnheld =     "|   |";
    public static final String fourBottomUnheld =     "|O O|";
    
    public static final String fiveTopUnheld =        "|O O|";
    public static final String fiveMiddleUnheld =     "| O |";
    public static final String fiveBottomUnheld =     "|O O|"; 
    
    public static final String sixTopUnheld =         "|O O|";
    public static final String sixMiddleUnheld =      "|O O|";
    public static final String sixBottomUnheld =      "|O O|";

    public static final String oneTopHeld =         "|   |";
    public static final String oneMiddleHeld =      "| X |";
    public static final String oneBottomHeld =      "|   |";

    public static final String twoTopHeld =         "|X  |";
    public static final String twoMiddleHeld =      "|   |";
    public static final String twoBottomHeld =      "|  X|";

    public static final String threeTopHeld =       "|X  |";
    public static final String threeMiddleHeld =    "| X |";
    public static final String threeBottomHeld =    "|  X|";

    public static final String fourTopHeld =        "|X X|";
    public static final String fourMiddleHeld =     "|   |";
    public static final String fourBottomHeld =     "|X X|";

    public static final String fiveTopHeld =        "|X X|";
    public static final String fiveMiddleHeld =     "| X |";
    public static final String fiveBottomHeld =     "|X X|";

    public static final String sixTopHeld =         "|X X|";
    public static final String sixMiddleHeld =      "|X X|";
    public static final String sixBottomHeld =      "|X X|";
    
    public static final String spacer =         "     ";

    public static final String[] topsUnheld = {null, oneTopUnheld, twoTopUnheld, threeTopUnheld, fourTopUnheld, fiveTopUnheld, sixTopUnheld};
    public static final String[] middlesUnheld = {null, oneMiddleUnheld, twoMiddleUnheld, threeMiddleUnheld, fourMiddleUnheld, fiveMiddleUnheld, sixMiddleUnheld};
    public static final String[] bottomUnheld = {null, oneBottomUnheld, twoBottomUnheld, threeBottomUnheld, fourBottomUnheld, fiveBottomUnheld, sixBottomUnheld};

    public static final String[] topsHeld = {null, oneTopHeld, twoTopHeld, threeTopHeld, fourTopHeld, fiveTopHeld, sixTopHeld};
    public static final String[] middlesHeld = {null, oneMiddleHeld, twoMiddleHeld, threeMiddleHeld, fourMiddleHeld, fiveMiddleHeld, sixMiddleHeld};
    public static final String[] bottomHeld = {null, oneBottomHeld, twoBottomHeld, threeBottomHeld, fourBottomHeld, fiveBottomHeld, sixBottomHeld};

    public static final String INVALID_INPUT = "Replaying input message.";

    public static String retrieveInput(List<String> validInput, String messageForUser) {
        return retrieveInput(validInput, messageForUser, INVALID_INPUT);
    }

    public static String retrieveInput(List<String> validInput, String messageForUser, String invalidInputMessage) {
        Scanner scanner = new Scanner(System.in);
        String inputToReturn = "";

        System.out.println(messageForUser);

        while (userInputIsNotValid(validInput, inputToReturn)) {
            inputToReturn = scanner.next();
            if (userInputIsNotValid(validInput, inputToReturn)) {
                System.out.printf("\"%s\" is not a valid option. %s%n", inputToReturn, invalidInputMessage);
                System.out.println(messageForUser);
            }
        }
        return inputToReturn;
    }

    private static boolean userInputIsNotValid(List<String> validInput, String inputToReturn) {
        return !validInput.contains(inputToReturn);
    }
}
