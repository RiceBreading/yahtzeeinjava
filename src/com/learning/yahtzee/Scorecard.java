package com.learning.yahtzee;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.learning.yahtzee.ScoreUtils.*;

public class Scorecard {

    private Dice ones;
    private Dice twos;
    private Dice threes;
    private Dice fours;
    private Dice fives;
    private Dice sixes;

    private Dice threeOfAKind;
    private Dice fourOfAKind;
    private Dice fullHouse;
    private Dice smallStraight;
    private Dice largeStraight;
    private Dice yahtzee;
    private Dice chance;

    public Scorecard() {
        ones = null;
        twos = null;
        threes = null;
        fours = null;
        fives = null;
        sixes = null;
        threeOfAKind = null;
        fourOfAKind = null;
        fullHouse = null;
        smallStraight = null;
        largeStraight = null;
        yahtzee = null;
        chance = null;
    }

    public Dice getOnes() {
        return ones;
    }

    public void setOnes(Dice ones) {
        this.ones = ones;
    }

    public Dice getTwos() {
        return twos;
    }

    public void setTwos(Dice twos) {
        this.twos = twos;
    }

    public Dice getThrees() {
        return threes;
    }

    public void setThrees(Dice threes) {
        this.threes = threes;
    }

    public Dice getFours() {
        return fours;
    }

    public void setFours(Dice fours) {
        this.fours = fours;
    }

    public Dice getFives() {
        return fives;
    }

    public void setFives(Dice fives) {
        this.fives = fives;
    }

    public Dice getSixes() {
        return sixes;
    }

    public void setSixes(Dice sixes) {
        this.sixes = sixes;
    }

    public Dice getThreeOfAKind() {
        return threeOfAKind;
    }

    public void setThreeOfAKind(Dice threeOfAKind) {
        this.threeOfAKind = threeOfAKind;
    }

    public Dice getFourOfAKind() {
        return fourOfAKind;
    }

    public void setFourOfAKind(Dice fourOfAKind) {
        this.fourOfAKind = fourOfAKind;
    }

    public Dice getFullHouse() {
        return fullHouse;
    }

    public void setFullHouse(Dice fullHouse) {
        this.fullHouse = fullHouse;
    }

    public Dice getSmallStraight() {
        return smallStraight;
    }

    public void setSmallStraight(Dice smallStraight) {
        this.smallStraight = smallStraight;
    }

    public Dice getLargeStraight() {
        return largeStraight;
    }

    public void setLargeStraight(Dice largeStraight) {
        this.largeStraight = largeStraight;
    }

    public Dice getYahtzee() {
        return yahtzee;
    }

    public void setYahtzee(Dice yahtzee) {
        this.yahtzee = yahtzee;
    }

    public Dice getChance() {
        return chance;
    }

    public void setChance(Dice chance) {
        this.chance = chance;
    }

    public int getScore() {
        return scoreOnes(ones)
                + scoreTwos(twos)
                + scoreThrees(threes)
                + scoreFours(fours)
                + scoreFives(fives)
                + scoreSixes(sixes)
                + scoreBonus(ones, twos, threes, fours, fives, sixes)
                + scoreThreeOfAKind(threeOfAKind)
                + scoreFourOfAKind(fourOfAKind)
                + scoreFullHouse(fullHouse)
                + scoreSmallStraight(smallStraight)
                + scoreLargeStraight(largeStraight)
                + scoreYahtzee(yahtzee)
                + scoreChance(chance);
    }

    public boolean gameIsNotOver() {
        return Stream.of(ones, twos, threes, fours, fives, sixes,
                threeOfAKind, fourOfAKind, fullHouse, smallStraight, largeStraight, yahtzee, chance)
                .anyMatch(Objects::isNull);
    }

    public List<String> retrieveAvailableScorecardOptions() {
        HashMap<String, Dice> options = new HashMap<>();
        options.put("0", new Dice());
        options.put("1", ones);
        options.put("2", twos);
        options.put("3", threes);
        options.put("4", fours);
        options.put("5", fives);
        options.put("6", sixes);
        options.put("7", threeOfAKind);
        options.put("8", fourOfAKind);
        options.put("9", fullHouse);
        options.put("10", smallStraight);
        options.put("11", largeStraight);
        options.put("12", yahtzee);
        options.put("13", chance);
        return options.entrySet().stream()
                .filter(stringDiceEntry -> stringDiceEntry.getValue() == null)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public String scorePrintout() {
        return String.format("Scorecard:%n" +
                        "    1: Ones: %s%n" +
                        "    2: Twos: %s%n" +
                        "    3: Threes: %s%n" +
                        "    4: Fours: %s%n" +
                        "    5: Fives: %s%n" +
                        "    6: Sixes: %s%n" +
                        "    Bonus: %s%n" +
                        "    7: Three of a Kind: %s%n" +
                        "    8: Four of a Kind: %s%n" +
                        "    9: Full House: %s%n" +
                        "    10: Small Straight: %s%n" +
                        "    11: Large Straight: %s%n" +
                        "    12: Yahtzee: %s%n" +
                        "    13: Chance: %s%n",
                Optional.ofNullable(ones).map(dice -> String.valueOf(scoreOnes(ones))).orElse("_"),
                Optional.ofNullable(twos).map(dice -> String.valueOf(scoreTwos(twos))).orElse("_"),
                Optional.ofNullable(threes).map(dice -> String.valueOf(scoreThrees(threes))).orElse("_"),
                Optional.ofNullable(fours).map(dice -> String.valueOf(scoreFours(fours))).orElse("_"),
                Optional.ofNullable(fives).map(dice -> String.valueOf(scoreFives(fives))).orElse("_"),
                Optional.ofNullable(sixes).map(dice -> String.valueOf(scoreSixes(sixes))).orElse("_"),
                scoreBonus(ones, twos, threes, fours, fives, sixes),
                Optional.ofNullable(threeOfAKind).map(dice -> String.valueOf(scoreThreeOfAKind(threeOfAKind))).orElse("_"),
                Optional.ofNullable(fourOfAKind).map(dice -> String.valueOf(scoreFourOfAKind(fourOfAKind))).orElse("_"),
                Optional.ofNullable(fullHouse).map(dice -> String.valueOf(scoreFullHouse(fullHouse))).orElse("_"),
                Optional.ofNullable(smallStraight).map(dice -> String.valueOf(scoreSmallStraight(smallStraight))).orElse("_"),
                Optional.ofNullable(largeStraight).map(dice -> String.valueOf(scoreLargeStraight(largeStraight))).orElse("_"),
                Optional.ofNullable(yahtzee).map(dice -> String.valueOf(scoreYahtzee(yahtzee))).orElse("_"),
                Optional.ofNullable(chance).map(dice -> String.valueOf(scoreChance(chance))).orElse("_"));
    }
}
