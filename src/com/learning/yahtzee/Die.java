package com.learning.yahtzee;

import java.util.Random;

public class Die {

    private Integer value;
    private boolean held;

    public Die() {
        this.held = false;
        roll();
    }

    public Die(Integer value) {
        this.held = false;
        this.value = value;
    }

    public void roll() {
        this.value = new Random().nextInt(5) + 1;
    }

    public void switchHold() {
        this.held = !this.held;
    }

    public void hold() {
        this.held = true;
    }

    public void unhold() {
        this.held = false;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public boolean isHeld() {
        return held;
    }

    public void setHeld(boolean held) {
        this.held = held;
    }

    public boolean canRoll() {
        return !isHeld();
    }
}
