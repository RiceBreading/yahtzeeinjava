package com.learning.yahtzee;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * This class represents the players collective dice.
 */
public class Dice {
    private final Die die1;
    private final Die die2;
    private final Die die3;
    private final Die die4;
    private final Die die5;

    public Dice() {
        die1 = new Die();
        die2 = new Die();
        die3 = new Die();
        die4 = new Die();
        die5 = new Die();
    }

    public Dice(int one, int two, int three, int four, int five) {
        die1 = new Die(one);
        die2 = new Die(two);
        die3 = new Die(three);
        die4 = new Die(four);
        die5 = new Die(five);
    }

    public Die getFirst() {
        return die1;
    }

    public Die getSecond() {
        return die2;
    }

    public Die getThird() {
        return die3;
    }

    public Die getFourth() {
        return die4;
    }

    public Die getFifth() {
        return die5;
    }

    public List<Die> getAllDice() {
        return Arrays.asList(die1, die2, die3, die4, die5);
    }

    public void switchHold(int selectedDie) {
        if(selectedDie == 1) {
            die1.switchHold();
        } else if (selectedDie == 2) {
            die2.switchHold();
        } else if (selectedDie == 3) {
            die3.switchHold();
        } else if (selectedDie == 4) {
            die4.switchHold();
        } else if (selectedDie == 5) {
            die5.switchHold();
        }
    }

    public void rollUnheld() {
        Stream.of(die1, die2, die3, die4, die5)
                .filter(Die::canRoll)
                .forEach(Die::roll);
    }

    public void unholdAll() {
        Stream.of(die1, die2, die3, die4, die5)
                .forEach(Die::unhold);
    }

    /**
     * Copy method to prevent referential integrity issues.
     */
    public Dice copy() {
        return new Dice(die1.getValue(), die2.getValue(), die3.getValue(), die4.getValue(), die5.getValue());
    }
}
