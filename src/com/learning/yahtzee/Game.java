package com.learning.yahtzee;

import java.util.Arrays;
import java.util.List;

import static com.learning.yahtzee.YahtzeeUtils.retrieveInput;

public class Game {
    public static final List<String> ONE_TWO = Arrays.asList("1", "2");
    public static final String WELCOME_MESSAGE = "Welcome to Yahtzee!";
    public static final String FIRST_DECISION = "Enter the number for the option that you would like:\n    1: New Game\n    2: Exit";

    public static void main(String[] args) {
        Yahtzee yahtzee = new Yahtzee();
        System.out.println(WELCOME_MESSAGE);

        switch (retrieveInput(ONE_TWO, FIRST_DECISION)) {
            case "1":
                yahtzee.play();
            case "2":
                System.exit(0);
        }
    }
}
