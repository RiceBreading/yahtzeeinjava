package com.learning.yahtzee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.learning.yahtzee.YahtzeeUtils.*;
import static java.lang.String.format;
import static java.lang.String.join;

public class Yahtzee {
    public static final String SELECT_DICE_FOR_HOLD = "Enter the number of the dice that you want to change the hold status of or 0 if you are done.\n%s";
    public static final String SELECT_SCORECARD_OPTION = "Select where you would like to place your dice.\n";
    public static final String INVALID_SCORE_OPTION_SELECTED = "Select a score option that you haven't already played.\n";
    public static final String SELECT_ROLL_OPTION = "Select an option.\n    1: Hold/Unhold Dice\n    2: Score Dice\n    3: Roll Again";
    public static final String SELECT_THIRD_ROLL_OPTION = "Select an option.\n    1: Hold/Unhold Dice\n    2: Score Dice";
    public static final List<String> ROLL_OPTIONS = Arrays.asList("1", "2", "3");
    public static final List<String> END_TURN_OPTIONS = Arrays.asList("1", "2");
    public static final List<String> DICE_HOLD_OPTIONS = Arrays.asList("0", "1", "2", "3", "4", "5");

    private final Scorecard scorecard = new Scorecard();
    private final Dice dice = new Dice();

    /**
     * Kicks off the game and runs to completion.
     */
    public void play() {
        clear();
        while (scorecard.gameIsNotOver()) {
            takeNextTurn();
        }
        clear();
        System.out.printf("Your final score is: %d%n", scorecard.getScore());
        printScorecard();
    }

    /**
     * Runs a three roll turn.
     */
    private void takeNextTurn() {
        for (int i = 0; i < 3; i++) {
            roll();
            printScorecard();
            System.out.printf("Here is roll %d:%n", i + 1);
            System.out.println(printDice());
            String userSelection = selectRollOptions(i);
            if ("1".equals(userSelection)) {
                selectDiceForHold();
            } else if ("2".equals(userSelection)) {
                break;
            }
            clear();
        }
        selectScorecard();
        resetTurn();
    }

    /**
     * Sets all dice to unheld and clears the console.
     */
    private void resetTurn() {
        dice.unholdAll();
        clear();
    }

    /**
     * Clears the console by adding a bunch of newlines.
     */
    private void clear() {
        System.out.printf("%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n%n");
    }

    /**
     * Determines which roll the user is on and gives the the correct options.
     */
    private String selectRollOptions(Integer roll) {
        if (roll < 2) {
            return retrieveInput(ROLL_OPTIONS, SELECT_ROLL_OPTION);
        }
        return retrieveInput(END_TURN_OPTIONS, SELECT_THIRD_ROLL_OPTION);
    }

    /**
     * Rolls all unheld dice.
     */
    private void roll() {
        dice.rollUnheld();
    }

    /**
     * Allows the user to select dice one at a time to hold.
     */
    private void selectDiceForHold() {
        while (true) {
            printDice();
            String selection = retrieveInput(DICE_HOLD_OPTIONS, format(SELECT_DICE_FOR_HOLD, printDice()));

            if ("0".equals(selection)) {
                break;
            }

            dice.switchHold(Integer.parseInt(selection));
        }
    }

    /**
     * Takes in the users selection and either displays the scorecard, or places the score appropriately.
     */
    private void selectScorecard() {
        List<String> inputs = new ArrayList<>(scorecard.retrieveAvailableScorecardOptions());
        inputs.add("0");
        while (true) {
            printScorecard();
            int input = Integer.parseInt(retrieveInput(inputs, SELECT_SCORECARD_OPTION, INVALID_SCORE_OPTION_SELECTED));
            if (input != 0) {
                if (input == 1) {
                    scorecard.setOnes(dice.copy());
                } else if (input == 2) {
                    scorecard.setTwos(dice.copy());
                } else if (input == 3) {
                    scorecard.setThrees(dice.copy());
                } else if (input == 4) {
                    scorecard.setFours(dice.copy());
                } else if (input == 5) {
                    scorecard.setFives(dice.copy());
                } else if (input == 6) {
                    scorecard.setSixes(dice.copy());
                } else if (input == 7) {
                    scorecard.setThreeOfAKind(dice.copy());
                } else if (input == 8) {
                    scorecard.setFourOfAKind(dice.copy());
                } else if (input == 9) {
                    scorecard.setFullHouse(dice.copy());
                } else if (input == 10) {
                    scorecard.setSmallStraight(dice.copy());
                } else if (input == 11) {
                    scorecard.setLargeStraight(dice.copy());
                } else if (input == 12) {
                    scorecard.setYahtzee(dice.copy());
                } else if (input == 13) {
                    scorecard.setChance(dice.copy());
                } else {
                    throw new RuntimeException("Couldn't determine scorecard slot.");
                }
                return;
            } else {
                printScorecard();
            }
        }
    }


    /**
     * Prints out all of the dice for the user to see.
     */
    private String printDice() {
        String topRow = join(spacer, retrieveTop(dice.getFirst()), retrieveTop(dice.getSecond()), retrieveTop(dice.getThird()), retrieveTop(dice.getFourth()), retrieveTop(dice.getFifth()));
        String middleRow = join(spacer, retrieveMiddle(dice.getFirst()), retrieveMiddle(dice.getSecond()), retrieveMiddle(dice.getThird()), retrieveMiddle(dice.getFourth()), retrieveMiddle(dice.getFifth()));
        String bottomRow = join(spacer, retrieveBottom(dice.getFirst()), retrieveBottom(dice.getSecond()), retrieveBottom(dice.getThird()), retrieveBottom(dice.getFourth()), retrieveBottom(dice.getFifth()));
        return join("\n", topRow, middleRow, bottomRow, "");
    }

    /**
     * Prints out the scorecard and values within.
     */
    private void printScorecard() {
        System.out.println(scorecard.scorePrintout());
    }

    /**
     * Retrieves the preset top of the dice for the given value.
     */
    private String retrieveTop(Die die) {
        if (die.isHeld()) {
            return topsHeld[die.getValue()];
        }
        return topsUnheld[die.getValue()];
    }

    /**
     * Retrieves the preset middle of the dice for the given value.
     */
    private String retrieveMiddle(Die die) {
        if (die.isHeld()) {
            return middlesHeld[die.getValue()];
        }
        return middlesUnheld[die.getValue()];
    }

    /**
     * Retrieves the preset bottom of the dice for the given value.
     */
    private String retrieveBottom(Die die) {
        if (die.isHeld()) {
            return bottomHeld[die.getValue()];
        }
        return bottomUnheld[die.getValue()];
    }
}
