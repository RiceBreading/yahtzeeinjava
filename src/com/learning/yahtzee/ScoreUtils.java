package com.learning.yahtzee;

public class ScoreUtils {
    /**
     * Method should return the score accumulated only by dice that have the value of 1
     */
    public static int scoreOnes(Dice dice) {
        //TODO write the code that looks at the Dice for ones and scores it correctly
        return 0;
    }

    /**
     * Method should return the score accumulated only by dice that have the value of 2
     */
    public static int scoreTwos(Dice dice) {
        //TODO write the code that looks at the Dice for twos and scores it correctly
        return 0;
    }

    /**
     * Method should return the score accumulated only by dice that have the value of 3
     */
    public static int scoreThrees(Dice dice) {
        //TODO write the code that looks at the Dice for threes and scores it correctly
        return 0;
    }

    /**
     * Method should return the score accumulated only by dice that have the value of 4
     */
    public static int scoreFours(Dice dice) {
        //TODO write the code that looks at the Dice for fours and scores it correctly
        return 0;
    }

    /**
     * Method should return the score accumulated only by dice that have the value of 5
     */
    public static int scoreFives(Dice dice) {
        //TODO write the code that looks at the Dice for fives and scores it correctly
        return 0;
    }

    /**
     * Method should return the score accumulated only by dice that have the value of 6
     */
    public static int scoreSixes(Dice dice) {
        //TODO write the code that looks at the Dice for sixes and scores it correctly
        return 0;
    }

    /**
     * Player should receive a bonus of 35 points if all of
     * the number sections above add up to a score of 63.
     *
     * If the number sections above do not add up to 63 then 
     * this method should return 0.
     */
    public static int scoreBonus(Dice ones, Dice twos, Dice threes, Dice fours, Dice fives, Dice sixes) {
        //TODO write the code that looks at the Dice for bonus eligibility and scores it correctly
        return 0;
    }

    /**
     * Player should receive the total added value of their dice if they have at least 3 of the same number.
     *
     * If they do not have at least 3 of the same number then they receive a score of 0.
     */
    public static int scoreThreeOfAKind(Dice dice) {
        //TODO write the code that looks at the Dice for a three of a kind and scores it correctly
        return 0;
    }

    /**
     * Player should receive the total added value of their dice if they have at least 4 of the same number.
     *
     * If they do not have at least 4 of the same number then they receive a score of 0.
     */
    public static int scoreFourOfAKind(Dice dice) {
        //TODO write the code that looks at the Dice for a four of a kind and scores it correctly
        return 0;
    }

    /**
     * Player should receive a score of 25 if there are exactly three of one number and two of a different number.
     *
     * If the conditions are not met then they receive a score of 0;
     */
    public static int scoreFullHouse(Dice dice) {
        //TODO write the code that looks at the Dice for a full house and scores it correctly
        return 0;
    }

    /**
     *  Player should receive a score of 30 if their dice have 4 consecutive numbers.
     *
     *  If they do not have 4 consecutive numbers then they receive a score of 0.
     */
    public static int scoreSmallStraight(Dice dice) {
        //TODO write the code that looks at the Dice for a small straight and scores it correctly
        return 0;
    }

    /**
     *  Player should receive a score of 40 if their dice contain 5 consecutive numbers.
     *
     *  If they do not have 5 consecutive numbers then they receive a score of 0.
     */
    public static int scoreLargeStraight(Dice dice) {
        //TODO write the code that looks at the Dice for a large straight and scores it correctly
        return 0;
    }

    /**
     * Player should receive a score of 50 if all 5 dice have the same value.
     *
     * If they do not have 5 dice of the same value then they receive a score of 0.
     */
    public static int scoreYahtzee(Dice dice) {
        //TODO write the code that looks at the Dice for a yahtzee and scores it correctly
        return 0;
    }

    /**
     * Player should receive a score that is the total added value of the dice regardless of their values.
     */
    public static int scoreChance(Dice dice) {
        //TODO write the code that looks at the Dice for chance and scores it correctly
        return 0;
    }
}
